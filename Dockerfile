FROM fedora:latest

WORKDIR /usr/src/app


# install packages
RUN dnf update -y && \
	dnf install nmap maven iputils -y

COPY pom.xml /usr/src/app

COPY /src /usr/src/app/src

COPY /.m2 /root/.m2
# check if maven cache works
# RUN ls /root/.m2/repository

# run maven package
RUN mvn package -DskipTests

ENV VERSION="not specified in docker run"
ENV REF="not specified in docker run"
ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dserver.port=${PORT} spring-boot:run" ]
