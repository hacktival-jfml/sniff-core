#!/usr/bin/env bash

my_pid="$(fuser 8080/tcp | grep -o '[^\:]*')"
if [[ -z $my_pid ]]
then
    echo -e  "\e[32mStarting Server\e[0m"
else
    echo -e "\e[31mPort 8080 taken py process $my_pid, aborting\e[0m"
    exit 1
fi


./run.sh &> /dev/null < /dev/null &
exit 0
