#!/usr/bin/env bash

if (("$1"<="0"))
then
    echo "first parameter must be greater than 0"
    exit 1;
fi
if [[ -z "$2" ]]
then
    echo "second parameter must be not empty"
    exit 1;
fi

i=$1;

while [[ "$i" -gt 0 ]];
do
    ((i--));
    msg="$(curl $2)"
    if [[ "$?" -eq 0 ]];
    then
        echo -e "\e[32mConnected to sever:\e[0m"
        echo "$msg"
        exit 0
    else
        echo -e "\e[33mServer not reachable, retrying (remaining: $i) \e[0m"
    fi
    sleep 5
done
echo -e "\e[31mServer not up!\e[0m"
exit 1
