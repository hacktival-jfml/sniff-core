package sniff.controller;

import lombok.var;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {
    @GetMapping("/api/version")
    public String getVersion() throws Exception {
        var version = System.getenv("VERSION");
        var res = System.getenv("REF");
        return "Version: " + version + "\ngit commit ref: " + res + "\n";
    }
}
