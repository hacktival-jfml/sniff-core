package sniff.controller;

import lombok.AllArgsConstructor;
import lombok.var;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import sniff.model.Device;
import sniff.model.DeviceRepository;
import sniff.model.DeviceStatus;
import sniff.model.PingStatus;
import sniff.model.web.DeviceBasicDTO;
import sniff.sniffer.EndpointSniffer;
import sniff.sniffer.Sniffer;
import sniff.sniffer.nmap.NMapService;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class SniffController {

    private Sniffer sniffer;

    private EndpointSniffer endpointSniffer;

    private DeviceRepository deviceRepository;

    private ModelMapper modelMapper;

    private NMapService scanner;

    @GetMapping("/api/sniff/now")
    @ResponseBody
    public void sniffNow() {
        sniffer.sniff();
        endpointSniffer.sniff();
    }

    @GetMapping("/api/sniff/search/{ip}")
    @ResponseBody
    public List<DeviceBasicDTO> search(@PathVariable String ip) throws Exception {
        var result = scanner.basicScan(ip, 24);
        return result.stream().map(x -> deviceRepository.findByIp(x).map(dev -> modelMapper.map(dev, DeviceBasicDTO.class))
                .orElse(DeviceBasicDTO.builder().ip(x).name("detected by nmap at " + new Date()).build()
                )).collect(Collectors.toList());
    }

    @GetMapping("/api/sniff/host/{ip}")
    @ResponseBody
    public List<String> scanHost(@PathVariable String ip) throws Exception {
        return scanner.scanHost(ip);
    }

    @GetMapping("/api/sniff/coke")
    @ResponseBody
    public ResponseEntity sniffCoke() {
        return new ResponseEntity("This incident will be reported.", HttpStatus.FORBIDDEN);
    }

    @GetMapping("/api/generate")
    @ResponseBody
    public void generateData() {
        Random random = new Random();
        var statusList = new ArrayList<DeviceStatus>();
        var device = new Device();
        device.setIp("192.168.172.42");
        device.setName("John Does Laptop");
        device.setStatus(statusList);
        ZonedDateTime baseTime = ZonedDateTime.of(2019, 5, 6, 0, 0, 0, 0, ZoneId.systemDefault());
        boolean last = true;
        for (int d = 0; d < 7; d++) {
            var dayTime = baseTime.plusDays(d);
            for (int h = 0; h < 24; h++) {
                var hourTime = dayTime.plusHours(h);
                for (int i = 0; i < 6; i++) {
                    var slotTime = hourTime.plusMinutes(i * 10);
                    var status = new DeviceStatus();
                    status.setDate(slotTime);
                    var randVal = random.nextInt(24);
                    if (randVal < h + 1 || randVal > 24 - h - 1) {
                        if (!last) {
                            randVal = random.nextInt(2);
                            if (randVal < 1) {
                                status.setStatus(PingStatus.offline);
                                last = false;
                            } else {
                                status.setStatus(PingStatus.online);
                                last = true;
                            }
                        } else {
                            status.setStatus(PingStatus.online);
                            last = true;
                        }
                    } else {
                        status.setStatus(PingStatus.offline);
                        last = false;
                    }
                    status.setDevice(device);
                    statusList.add(status);
                }
            }
        }
        deviceRepository.save(device);
    }
}
