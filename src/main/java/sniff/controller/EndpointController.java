package sniff.controller;

import lombok.AllArgsConstructor;
import lombok.var;
import org.assertj.core.util.Strings;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import sniff.model.Endpoint;
import sniff.model.EndpointRepository;
import sniff.model.EndpointStatusRepository;
import sniff.model.web.DeviceTimeRequest;
import sniff.model.web.EndpointBasicDTO;
import sniff.model.web.EndpointDetailedResponse;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@AllArgsConstructor
public class EndpointController {
    private EndpointRepository endpointRepository;
    private EndpointStatusRepository endpointStatusRepository;
    private ModelMapper modelMapper;

    @GetMapping("/api/endpoint")
    @ResponseBody
    public List<EndpointBasicDTO> getAllEndpoints() {
        Iterable<Endpoint> devices = endpointRepository.findAll();
        return StreamSupport.stream(devices.spliterator(), false).map(d -> modelMapper.map(d, EndpointBasicDTO.class)).collect(Collectors.toList());
    }

    @GetMapping("/api/endpoint/{id}")
    @ResponseBody
    public EndpointDetailedResponse getEndpoint(@PathVariable long id) {
        Optional<Endpoint> device = endpointRepository.findById(id);
        return device.map(endpoint -> modelMapper.map(endpoint, EndpointDetailedResponse.class)).orElse(null);
    }

    @PostMapping("/api/endpoint/time")
    @ResponseBody
    public EndpointDetailedResponse getEndpointFiltered(@RequestBody DeviceTimeRequest request) {
        if (request == null || request.getDeviceId() == null) {
            return null;
        }
        return endpointRepository.findById(request.getDeviceId()).map(x -> {
            var status = endpointStatusRepository.filterByDate(x, request.getStartTime(), request.getEndTime());
            x.setStatus(status);
            return x;
        }).map(x -> modelMapper.map(x, EndpointDetailedResponse.class))
                .orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND, "endpoint with id " + request.getDeviceId() + " not found"));
    }


    @PutMapping("/api/endpoint")
    @ResponseBody
    public EndpointBasicDTO addEndpoint(@RequestBody EndpointBasicDTO dto) {
        var result = endpointRepository.save(modelMapper.map(dto, Endpoint.class));
        return modelMapper.map(result, EndpointBasicDTO.class);
    }

    @PostMapping("/api/endpoint/update/{id}")
    @ResponseBody
    public EndpointBasicDTO updateDevice(@RequestBody EndpointBasicDTO dto, @PathVariable long id) {
        var endpoint = endpointRepository.findById(id);
        return endpoint.map(x -> {
            if (!Strings.isNullOrEmpty(dto.getMethod())) {
                x.setMethod(dto.getMethod());
            }
            if (!Strings.isNullOrEmpty(dto.getName())) {
                x.setName(dto.getName());
            }
            if (!Strings.isNullOrEmpty(dto.getUrl())) {
                x.setUrl(dto.getUrl());
            }
            return modelMapper.map(endpointRepository.save(x), EndpointBasicDTO.class);

        }).orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND, "endpoint with id " + id + " not found"));
    }

    @DeleteMapping("/api/endpoint/{id}")
    @ResponseBody
    public void deleteEndpoint(@PathVariable long id) {
        endpointRepository.deleteById(id);
    }
}
