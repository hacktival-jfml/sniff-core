package sniff.controller;

import lombok.AllArgsConstructor;
import lombok.var;
import org.assertj.core.util.Strings;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import sniff.model.*;
import sniff.model.web.DeviceBasicDTO;
import sniff.model.web.DeviceDetailedResponse;
import sniff.model.web.DeviceTimeRequest;
import sniff.sniffer.nmap.NMapService;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
@AllArgsConstructor
public class DeviceController {

    private DeviceRepository deviceRepository;

    private DeviceStatusRepository deviceStatusRepository;

    private ModelMapper modelMapper;

    private NMapService nMapService;

    @GetMapping("/api/device")
    @ResponseBody
    public List<DeviceBasicDTO> getAllDevices() {
        Iterable<Device> devices = deviceRepository.findAll();
        return StreamSupport.stream(devices.spliterator(), false).map(d -> modelMapper.map(d, DeviceBasicDTO.class)).collect(Collectors.toList());
    }

    @GetMapping("/api/device/auto-discover/{ip}")
    @ResponseBody
    public List<DeviceBasicDTO> autoDiscover(@PathVariable String ip) throws Exception {
        var ipsDiscovered = nMapService.basicScan(ip, 24);
        List<Device> devices = ipsDiscovered.stream()
                .map(x -> deviceRepository.findByIp(x).orElse(Device.builder().ip(x).name("auto-discovered at " + new Date()).build()))
                .peek(x -> {
                    x.getStatus().add(DeviceStatus.builder().date(ZonedDateTime.now()).device(x).status(PingStatus.online).build());
                    deviceRepository.save(x);
                }).collect(Collectors.toList());
        return devices.stream().map(x -> modelMapper.map(x, DeviceBasicDTO.class)).collect(Collectors.toList());
    }

    @GetMapping("/api/device/{id}")
    @ResponseBody
    public DeviceDetailedResponse getDevice(@PathVariable long id) {
        Optional<Device> device = deviceRepository.findById(id);
        if (device.isPresent()) {
            var deviceDTO = modelMapper.map(device.get(), DeviceDetailedResponse.class);
            return deviceDTO;
        }
        return null;
    }

    @PostMapping("/api/device/time")
    @ResponseBody
    public DeviceDetailedResponse getDeviceFiltered(@RequestBody DeviceTimeRequest request) {
        if (request == null || request.getDeviceId() == null) {
            return null;
        }
        return deviceRepository.findById(request.getDeviceId()).map(d -> {
            var status = deviceStatusRepository.filterByDate(d, request.getStartTime(), request.getEndTime());
            d.setStatus(status);
            return d;
        }).map(d -> modelMapper.map(d, DeviceDetailedResponse.class)).orElseThrow(() -> new HttpServerErrorException(HttpStatus.NOT_FOUND, "device with id " + request.getDeviceId() + " not found"));
    }

    @PutMapping("/api/device")
    @ResponseBody
    public DeviceBasicDTO addDevice(@RequestBody DeviceBasicDTO device) {
        deviceRepository.save(modelMapper.map(device, Device.class));
        var dbDevice = deviceRepository.findByIp(device.getIp());
        if (dbDevice.isPresent()) {
            var deviceDTO = modelMapper.map(dbDevice.get(), DeviceBasicDTO.class);
            return deviceDTO;
        }
        return null;
    }

    @PostMapping("/api/device/update/{id}")
    @ResponseBody
    public DeviceBasicDTO updateDevice(@RequestBody DeviceBasicDTO deviceDTO, @PathVariable long id) {
        var optionalDevice = deviceRepository.findById(id);
        if (optionalDevice.isPresent()) {
            var dbDevice = optionalDevice.get();
            if (!Strings.isNullOrEmpty(deviceDTO.getIp())) {
                dbDevice.setIp(deviceDTO.getIp());
            }
            if (!Strings.isNullOrEmpty(deviceDTO.getName())) {
                dbDevice.setName(deviceDTO.getName());
            }
            deviceRepository.save(dbDevice);
            return modelMapper.map(dbDevice, DeviceBasicDTO.class);
        }
        return null;
    }

    @DeleteMapping("/api/device/{id}")
    @ResponseBody
    public void deleteDevice(@PathVariable long id) {
        deviceRepository.deleteById(id);
    }
}
