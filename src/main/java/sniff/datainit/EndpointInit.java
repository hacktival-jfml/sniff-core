package sniff.datainit;

import lombok.AllArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import sniff.model.Endpoint;
import sniff.model.EndpointRepository;

@Component
@AllArgsConstructor
public class EndpointInit implements ApplicationRunner {

    private EndpointRepository endpointRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = endpointRepository.count();

        if (count == 0) {
            Endpoint endpoint = Endpoint.builder().name("google.com").url("http://google.com").method("GET").build();
            endpointRepository.save(endpoint);
        }
    }
}
