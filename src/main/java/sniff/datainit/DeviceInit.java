package sniff.datainit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import sniff.model.Device;
import sniff.model.DeviceRepository;

@Component
public class DeviceInit implements ApplicationRunner {

    private DeviceRepository deviceRepository;

    @Autowired
    public DeviceInit(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = deviceRepository.count();

        if (count == 0) {
            addDevice("1.1.1.1", "Internet");
        }
    }

    private void addDevice(String ip, String name) {
        Device device = Device.builder().ip(ip).name(name).build();
        deviceRepository.save(device);
    }
}
