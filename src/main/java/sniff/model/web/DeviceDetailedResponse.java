package sniff.model.web;

import lombok.Data;

import java.util.List;

@Data
public class DeviceDetailedResponse {

    private Long deviceId;
    private String ip;
    private String name;
    private List<DeviceStatusResponse> status;
}
