package sniff.model.web;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import sniff.model.PingStatus;

import java.time.ZonedDateTime;

@Data
public class DeviceStatusResponse {
    private PingStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS[xxx]")
    private ZonedDateTime date;
}
