package sniff.model.web;

import lombok.Data;

import java.util.List;

@Data
public class EndpointDetailedResponse {

    private Long endpointId;
    private String url;
    private String name;
    private String method;
    private List<EndpointStatusResponse> status;

}
