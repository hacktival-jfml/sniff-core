package sniff.model.web;

import lombok.Data;

@Data
public class EndpointBasicDTO {

    private Long endpointId;
    private String url;
    private String name;
    private String method;

}
