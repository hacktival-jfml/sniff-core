package sniff.model.web;

import lombok.Data;

import java.time.ZonedDateTime;

@Data
public class DeviceTimeRequest {

    private Long deviceId;
    private ZonedDateTime startTime;
    private ZonedDateTime endTime;

}
