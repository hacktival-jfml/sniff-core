package sniff.model;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "STATUS")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class DeviceStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    @NonNull
    private PingStatus status;
    @ManyToOne
    private Device device;
    @Column
    @NonNull
    private ZonedDateTime date;
}
