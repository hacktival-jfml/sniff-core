package sniff.model;

public enum PingStatus {
    online,
    offline,
}
