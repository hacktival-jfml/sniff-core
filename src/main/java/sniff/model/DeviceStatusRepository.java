package sniff.model;

import org.springframework.data.repository.CrudRepository;

import java.time.ZonedDateTime;
import java.util.List;

public interface DeviceStatusRepository extends CrudRepository<DeviceStatus, Long> {
    List<DeviceStatus> findAllByDeviceAndDateIsGreaterThanEqualAndDateIsLessThanEqual(Device device, ZonedDateTime lower, ZonedDateTime upper);

    List<DeviceStatus> findAllByDeviceAndDateIsGreaterThanEqual(Device device, ZonedDateTime lower);

    List<DeviceStatus> findAllByDeviceAndDateIsLessThanEqual(Device device, ZonedDateTime upper);

    List<DeviceStatus> findAllByDevice(Device device);

    default List<DeviceStatus> filterByDate(Device device, ZonedDateTime lower, ZonedDateTime upper) {
        if (lower == null && upper == null) {
            return findAllByDevice(device);
        } else if (lower != null && upper != null) {
            return findAllByDeviceAndDateIsGreaterThanEqualAndDateIsLessThanEqual(device, lower, upper);
        } else if (lower != null) {
            return findAllByDeviceAndDateIsGreaterThanEqual(device, lower);
        } else {
            return findAllByDeviceAndDateIsLessThanEqual(device, upper);
        }
    }
}
