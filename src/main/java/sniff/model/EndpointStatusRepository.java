package sniff.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;

@Repository
public interface EndpointStatusRepository extends CrudRepository<EndpointStatus, Long> {

    List<EndpointStatus> findAllByEndpointAndDateIsGreaterThanEqualAndDateIsLessThanEqual(Endpoint endpoint, ZonedDateTime lower, ZonedDateTime upper);

    List<EndpointStatus> findAllByEndpointAndDateIsGreaterThanEqual(Endpoint endpoint, ZonedDateTime lower);

    List<EndpointStatus> findAllByEndpointAndDateIsLessThanEqual(Endpoint endpoint, ZonedDateTime upper);

    List<EndpointStatus> findAllByEndpoint(Endpoint endpoint);

    default List<EndpointStatus> filterByDate(Endpoint endpoint, ZonedDateTime lower, ZonedDateTime upper) {
        if (lower == null && upper == null) {
            return findAllByEndpoint(endpoint);
        } else if (lower != null && upper != null) {
            return findAllByEndpointAndDateIsGreaterThanEqualAndDateIsLessThanEqual(endpoint, lower, upper);
        } else if (lower != null) {
            return findAllByEndpointAndDateIsGreaterThanEqual(endpoint, lower);
        } else {
            return findAllByEndpointAndDateIsLessThanEqual(endpoint, upper);
        }
    }
}
