package sniff.model;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
@Table(name = "ENDPOINT_STATUS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class EndpointStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column
    @NonNull
    private String statusMessage;
    @Column
    private int statusCode;
    @ManyToOne
    private Endpoint endpoint;
    @Column
    @NonNull
    private ZonedDateTime date;
}
