package sniff.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ENDPOINT")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Endpoint {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long endpointId;
    @Column
    private String url;
    @Column
    private String name;
    @Column
    private String method;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "endpoint", cascade = CascadeType.ALL)
    private List<EndpointStatus> status;
}
