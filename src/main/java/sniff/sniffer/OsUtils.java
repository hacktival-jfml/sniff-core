package sniff.sniffer;

import org.springframework.stereotype.Service;

@Service
public class OsUtils {
    private String OS = null;

    public String getOsName() {
        if (OS == null) {
            OS = System.getProperty("os.name");
        }
        return OS;
    }

    public boolean isWindows() {
        return getOsName().startsWith("Windows");
    }

}