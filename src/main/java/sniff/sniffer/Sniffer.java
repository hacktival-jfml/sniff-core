package sniff.sniffer;


import lombok.var;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sniff.model.DeviceRepository;
import sniff.model.DeviceStatus;
import sniff.model.PingStatus;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.ZonedDateTime;

@Component
public class Sniffer {

    private final Logger logger = LoggerFactory.getLogger(Sniffer.class);

    @Autowired
    OsUtils osUtils;
    @Autowired
    DeviceRepository deviceRepository;

    private int ping(String address) throws IOException, InterruptedException {
        String count = " -c 1 ";
        if (osUtils.isWindows()) {
            count = " -n 1 ";
        }

        int result = Runtime.getRuntime().exec("ping " + count + address).waitFor();
        logger.debug(("Pinged " + address + " with result " + result + " ;is windows: " + osUtils.isWindows()));
        return result;
    }

    @Scheduled(fixedDelayString = "${options.sniffer.delay.device}", initialDelay = 2000)
    @Transactional
    public void sniff() {
        var devices = deviceRepository.findAll();
        devices.forEach(device -> {
            Hibernate.initialize(device.getStatus());
            var status = new DeviceStatus();
            status.setDevice(device);
            status.setDate(ZonedDateTime.now());
            try {
                if (ping(device.getIp()) == 0) {
                    logger.debug("Sniffed address is reachable");
                    status.setStatus(PingStatus.online);
                } else {
                    logger.debug("Sniffed address is not reachable");
                    status.setStatus(PingStatus.offline);
                }
            } catch (Exception e) {
                status.setStatus(PingStatus.offline);
                logger.error("Error during");
                e.printStackTrace();
            }
            device.getStatus().add(status);
            deviceRepository.save(device);
        });
    }
}
