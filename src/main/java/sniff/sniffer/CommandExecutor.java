package sniff.sniffer;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

@AllArgsConstructor
@Service
public class CommandExecutor {

    private OsUtils osUtils;

    public boolean hasCommand(String command) {
        try {
            if (osUtils.isWindows()) {
                return Runtime.getRuntime().exec("WHERE " + command).waitFor() == 0;
            } else {
                return Runtime.getRuntime().exec("hash " + command).waitFor() == 0;
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<String> execute(String command) throws IOException, InterruptedException {
        Process proc = Runtime.getRuntime().exec(command);
        BufferedReader read = new BufferedReader(new InputStreamReader(proc.getInputStream()));
        proc.waitFor();
        List<String> result = new LinkedList<>();
        while (read.ready()) {
            result.add(read.readLine());
        }
        return result;
    }
}
