package sniff.sniffer;

import lombok.AllArgsConstructor;
import lombok.var;
import org.hibernate.Hibernate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sniff.model.EndpointRepository;
import sniff.model.EndpointStatus;

import javax.transaction.Transactional;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.ZonedDateTime;

@Component
@AllArgsConstructor
public class EndpointSniffer {
    private EndpointRepository endpointRepository;

    @Scheduled(fixedDelayString = "${options.sniffer.delay.endpoint}", initialDelay = 2000)
    @Transactional
    public void sniff() {
        var endpoints = endpointRepository.findAll();
        endpoints.forEach(endpoint -> {
            Hibernate.initialize(endpoint.getStatus());
            var status = new EndpointStatus();
            status.setEndpoint(endpoint);
            status.setDate(ZonedDateTime.now());
            try {
                URL url = new URL(endpoint.getUrl());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod(endpoint.getMethod());
                status.setStatusCode(con.getResponseCode());
                status.setStatusMessage(con.getResponseMessage());

            } catch (Exception e) {
                status.setStatusCode(-1);
                status.setStatusMessage("Error");
            }
            endpoint.getStatus().add(status);
            endpointRepository.save(endpoint);
        });
    }
}
