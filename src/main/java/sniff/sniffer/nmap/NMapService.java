package sniff.sniffer.nmap;

import lombok.AllArgsConstructor;
import lombok.var;
import org.springframework.stereotype.Service;
import sniff.sniffer.CommandExecutor;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class NMapService {

    private static final String NMAP_CMD = "nmap";
    private static final String IP_REGEX = ".*\\b((\\d{1,3}\\.){3}\\d{1,3})\\b.*";

    private CommandExecutor commandExecutor;

    public boolean hasNMap() {
        return commandExecutor.hasCommand(NMAP_CMD);
    }

    private List<String> scan(String parameters, String ip) throws IOException, InterruptedException {
        if (!hasNMap()) {
            throw new RuntimeException("nmap not found, no scan possible on host!");
        }
        return commandExecutor.execute(NMAP_CMD + " " + parameters + " " + ip);
    }

    private List<String> scan(String parameters, String ip, int range) throws IOException, InterruptedException {
        return scan(parameters, ip + "/" + range);
    }

    public List<String> scanHost(String ip) throws Exception {
        return scan("-sS -O", ip);
    }

    public List<String> basicScan(String ip, int range) throws IOException, InterruptedException {
        var result = scan("-sn", ip, range);

        return result.stream().map(x -> {
            Pattern pattern = Pattern.compile(IP_REGEX);
            Matcher matcher = pattern.matcher(x);
            if (matcher.find()) {
                return matcher.group(1);
            } else {
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
